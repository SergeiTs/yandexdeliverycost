package ru.yandex.delivery;

public enum Distance {
    TWO_KM(2.0),
    TEN_KM(10.0),
    THIRTY_KM(30.0);

    private double distance;

    Distance(double distance) {
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

}
