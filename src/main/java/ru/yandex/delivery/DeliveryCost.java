package ru.yandex.delivery;

import java.text.DecimalFormat;

public class DeliveryCost {

    public double calculateCost(double distance, Pack pack, Workload workload) throws DeliveryException {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        if (pack.isFragile() && distance > 30) {
            throw new DeliveryException("Fragile pack can't delivered over 30km");
        }
        double cost = calculateDistanceCost(distance);

        if (pack.isFragile()) {
            cost += 300;
        }

        if (pack.isLarge()) {
            cost += 200;
        } else {
            cost += 100;
        }

        cost *= workload.getIndex();

        if (cost < 400) {
            cost = 400;
        }

        return Double.parseDouble(decimalFormat.format(cost));
    }

    private int calculateDistanceCost(double distance) {
        if (distance < Distance.TWO_KM.getDistance()) {
            return 50;
        } else if (distance < Distance.TEN_KM.getDistance()) {
            return 100;
        } else if (distance < Distance.THIRTY_KM.getDistance()) {
            return 200;
        } else {
            return 300;
        }
    }

}
