package ru.yandex.delivery;

public class Pack {

    private boolean large;
    private boolean fragile;

    public Pack(boolean large, boolean fragile) {
        this.large = large;
        this.fragile = fragile;
    }

    public boolean isLarge() {
        return large;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setLarge(boolean large) {
        this.large = large;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    @Override
    public String toString() {
        return "Pack{" +
                "large=" + large +
                ", fragile=" + fragile +
                '}';
    }

}
