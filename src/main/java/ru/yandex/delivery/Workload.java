package ru.yandex.delivery;

public enum Workload {

    HIGHEST(1.6),
    HIGHER(1.4),
    HIGH(1.2),
    USUAL(1);

    private double index;

    Workload(double index) {
        this.index = index;
    }

    public double getIndex() {
        return index;
    }

}
