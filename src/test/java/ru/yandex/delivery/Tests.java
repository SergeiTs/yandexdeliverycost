package ru.yandex.delivery;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class Tests {

    private DeliveryCost deliveryCost = new DeliveryCost();

    @ParameterizedTest
    @MethodSource("dataProvider")
    void checkDeliveryCost(double distance, Pack pack, Workload workload, double cost) {
        try {
            assertEquals(cost, deliveryCost.calculateCost(distance, pack, workload));
        } catch (DeliveryException e) {
            System.err.println(e);
        }
    }

    static Stream<Arguments> dataProvider() {
        return Stream.of(
                arguments(1, new Pack(true, true), Workload.USUAL, 550),
                arguments(7, new Pack(true, true), Workload.USUAL, 600),
                arguments(15, new Pack(true, true), Workload.USUAL, 700),
                arguments(33, new Pack(true, false), Workload.USUAL, 500),
                arguments(33, new Pack(false, false), Workload.USUAL, 400),
                arguments(33, new Pack(false, false), Workload.USUAL, 400),
                arguments(33, new Pack(false, false), Workload.HIGH, 480),
                arguments(33, new Pack(false, false), Workload.HIGHER, 560),
                arguments(33, new Pack(false, false), Workload.HIGHEST, 640)
        );
    }

    @Test
    void checkFragileException() {
        DeliveryException exception = assertThrows(DeliveryException.class, () ->
                deliveryCost.calculateCost(50, new Pack(true, true), Workload.USUAL));
        assertEquals("Fragile pack can't delivered over 30km", exception.getMessage());
    }

}
